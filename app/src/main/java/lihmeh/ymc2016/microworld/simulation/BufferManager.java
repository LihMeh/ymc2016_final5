package lihmeh.ymc2016.microworld.simulation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mihailpolovnev on 26/11/16.
 */

public final class BufferManager {

    private final List<boolean[]> buffers;
    private int currentBuffer = -1;

    public BufferManager(int width, int height, int bufferCount) {
        assert(bufferCount>1);
        buffers = new ArrayList<boolean[]>(bufferCount);
        for (int bufferIdx=0;bufferIdx<bufferCount;bufferIdx++) {
            boolean[] currentBuff = new boolean[width * height];
            buffers.add(currentBuff);
        }
    }

    public boolean[] getNext() {
        currentBuffer++;
        if (currentBuffer >= buffers.size()) currentBuffer = 0;
        return buffers.get(currentBuffer);
    }

}
