package lihmeh.ymc2016.microworld.simulation;

import java.util.Random;

/**

 В каждом шестиугольнике живёт или не живёт микроб.
 Поколения микробов сменяют друг друга одно за одним. Новое поколение зависит от того, каким было предыдущее.
 Если рядом с пустой клеткой живут ровно два микроба, при смене поколения в ней зарождается жизнь.
 Если у микроба есть три или четыре соседа, он переживает смену поколений. Если соседей меньше или больше — при смене поколений он умирает от тесноты или от огорчения.

 */

public final class World {

    private final int width;
    private final int height;
    private final BufferManager bufferManager;

    private boolean[] currentState;

    public World(final int width, final int height) {
        this.width = width;
        this.height = height;
        bufferManager = new BufferManager(width, height, 2);
        initWorld();
    }

    private void initWorld() {
        final Random rand = new Random();
        currentState = bufferManager.getNext();
        for (int idx=0;idx<currentState.length;idx++) {
            currentState[idx] = rand.nextBoolean();
        }
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getIdx(final int row, final int col) {
        return row*width+col;
    }

    public boolean isAlive(final int row, final int col) {
        if (row<0) return false;
        if (row>=width) return false;
        if (col<0) return false;
        if (col>=height) return false;
        return currentState[getIdx(row, col)];
    }

    public void simulateStep() {
        final boolean[] nextState = bufferManager.getNext();

        for (int col=0;col<width;col++) {
            for (int row=0;row<width;row++) {

                int bufferIdx = getIdx(row, col);
                int neighbors = countHeightbors(row, col);

                if (currentState[bufferIdx]) {
                    // It's alive
                    nextState[bufferIdx] = (neighbors==3)||(neighbors==4);
                } else {
                    // It's not alive
                    nextState[bufferIdx] = (neighbors==2);
                }

            }
        }

        currentState = nextState;
    }

    private int countHeightbors(int row, int col) {
        int sum = 0;
        for (int r=row-1;r<=row+1;r++) {
            for (int c=col-1;c<=col+1;c++) {
                if (isAlive(r,c)) sum++;
            }
        }
        if (isAlive(row, col)) sum--;
        return sum;
    }

    public void change(int row, int col) {
        int idx = getIdx(row, col);
        if (idx<0) return;
        if (idx>=currentState.length) return;
        currentState[idx] = !currentState[idx];
    }

}
