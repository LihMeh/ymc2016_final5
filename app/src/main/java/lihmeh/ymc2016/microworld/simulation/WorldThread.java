package lihmeh.ymc2016.microworld.simulation;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.SurfaceHolder;

import java.util.Random;

import lihmeh.ymc2016.microworld.MainActivity;

/**
 * Created by mihailpolovnev on 26/11/16.
 */

public class WorldThread extends  Thread {

    private final static int STEP_TIMEOUT = 1000;
    private final static float MARGIN = 1;

    private volatile boolean runFlag = false;
    private SurfaceHolder surfaceHolder;
    private final World world;

    private volatile float scale = 1.f;

    private final WorldSurfaceView parentView;

    public WorldThread(WorldSurfaceView worldSurfaceView, World world) {
        parentView = worldSurfaceView;
        this.surfaceHolder = surfaceHolder;
        this.world = world;
    }

    public void setRunning(boolean run) {
        runFlag = run;
    }

    public boolean isRunning() {
        return runFlag;
    }

    @Override
    public void run() {
        while (runFlag) {
            try {
                parentView.drawWorldThreadSafe();
                world.simulateStep();
            }
            finally {

            }
            try {
                Thread.sleep(STEP_TIMEOUT);
            }catch (InterruptedException ex) {}

        }
    }



    public void setScale(float scale) {
        this.scale = scale;
    }



}
