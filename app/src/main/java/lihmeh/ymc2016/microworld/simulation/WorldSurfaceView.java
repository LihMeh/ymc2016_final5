package lihmeh.ymc2016.microworld.simulation;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.ScaleGestureDetector.SimpleOnScaleGestureListener;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

import lihmeh.ymc2016.microworld.MainActivity;

/**
 * Created by mihailpolovnev on 26/11/16.
 */

public class WorldSurfaceView extends SurfaceView implements SurfaceHolder.Callback {

    private final float MARGIN = 1;

    private WorldThread worldThread = null;

    private World world = null;

    private ScaleGestureDetector scaleGestureDetector;
    private GestureDetector gestureDetector;

    MainActivity activity = null;

    public void init(Context context) {

        if (context instanceof  MainActivity) {
            activity = (MainActivity)context;
        }

        getHolder().addCallback(this);
        scaleGestureDetector = new ScaleGestureDetector(context, new ScaleListener());
        gestureDetector = new GestureDetector(context, new MoveListener());

        world = new World(32, 32);
    }

    private float posX = 0;
    private float posY = 0;
    private float scaleFactor = 1;

    public WorldSurfaceView(Context context) {
        super(context);
        init(context);
    }

    public WorldSurfaceView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(context);
    }

    public WorldSurfaceView(Context context, AttributeSet attributeSet, int intVal) {
        super(context, attributeSet, intVal);
        init(context);
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        if (worldThread!=null) pause();
    }

    public boolean isRunning() {
        return (worldThread!=null)&&(worldThread.isRunning());
    }

    public void pause() {
        if (!isRunning()) return;
        worldThread.setRunning(false);
        try {worldThread.join();} catch (InterruptedException ex) {};
        worldThread = null;
    }

    public void start() {
        if (isRunning()) return;
        worldThread = new WorldThread(this, world);
        worldThread.setRunning(true);
        worldThread.start();
    }


    private class ScaleListener extends SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            scaleFactor *= detector.getScaleFactor();
            drawWorld();
            invalidate();
            return true;
        }
    }

    private class MoveListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            posX -= distanceX;
            posY -= distanceY;
            drawWorld();
            invalidate();
            return true;
        }

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            if (isRunning()) return true;

            float tappedX = e.getX();
            float tappedY = e.getY();

            final float itemSize = scaleFactor*((float)WorldSurfaceView.this.getWidth())/world.getWidth();

            int itemRow =  (int)((tappedX-posX)/itemSize);
            int itemCol = (int)((tappedY-posY)/itemSize);
            world.change(itemRow, itemCol);



            drawWorld();
            invalidate();
            return true;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        scaleGestureDetector.onTouchEvent(event);
        gestureDetector.onTouchEvent(event);
        return true;
    }

    public void drawWorldThreadSafe() {
        if (activity!=null) {
            activity.runOnUiThread(drawHelper);
        }
    }

    private void drawWorld() {
        final SurfaceHolder surfaceHolder = getHolder();
        if (surfaceHolder==null) return;
        Canvas canvas = null;
        try {
            canvas = surfaceHolder.lockCanvas(null);
            drawWorldInner(canvas);
        } finally {
            if (canvas!=null) surfaceHolder.unlockCanvasAndPost(canvas);
        }
    }

    private void drawWorldInner(Canvas canvas) {
        final float scale = this.scaleFactor;
        final float itemSize = scale*((float)canvas.getWidth())/world.getWidth();

        Paint fill = new Paint();
        fill.setColor(Color.GRAY);
        fill.setStyle(Paint.Style.FILL);
        canvas.drawRect(0,0,canvas.getWidth(), canvas.getHeight(), fill);


        Paint paintAlive = new Paint();
        paintAlive.setColor(Color.BLUE);
        paintAlive.setStyle(Paint.Style.FILL);
        Paint paintDead = new Paint();
        paintDead.setColor(Color.BLACK);
        paintDead.setStyle(Paint.Style.FILL);
        synchronized (world) {
            int width = world.getWidth();
            int height = world.getHeight();
            for (int col=0;col<width;col++) {
                for (int row=0;row<height;row++) {
                    Paint paint = world.isAlive(row, col)?paintAlive:paintDead;
                    canvas.drawRect(itemSize*row + MARGIN + posX,
                            itemSize*col + MARGIN +posY,
                            itemSize*(row+1) - MARGIN +posX,
                            itemSize*(col+1) - MARGIN + posY,
                            paint);
                }
            }
        }
    }

    private Runnable drawHelper = new Runnable() {
        @Override
        public void run() {
            drawWorld();
        }
    };
}
