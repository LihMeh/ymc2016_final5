package lihmeh.ymc2016.microworld;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import lihmeh.ymc2016.microworld.simulation.World;
import lihmeh.ymc2016.microworld.simulation.WorldSurfaceView;

public class MainActivity extends AppCompatActivity {



    private WorldSurfaceView worldSurfaceView;

    private MenuItem startBtn = null;
    private MenuItem stopBtn = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        worldSurfaceView = (WorldSurfaceView)findViewById(R.id.surfaceView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.mainmenu, menu);
        startBtn = menu.findItem(R.id.btnStart);
        stopBtn = menu.findItem(R.id.btnPause);
        refreshMenu();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.btnStart:
                start();
                break;
            case R.id.btnPause:
                pause();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        refreshMenu();
        return true;
    }

    private void refreshMenu() {
        startBtn.setVisible(!worldSurfaceView.isRunning());
        stopBtn.setVisible(worldSurfaceView.isRunning());
    }


    private void pause() {
        worldSurfaceView.pause();
    }

    private void start() {
        worldSurfaceView.start();
    }

}
